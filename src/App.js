import React, { Component } from "react";
import { Provider } from "react-redux";
import Routes from "./util/route";
import store from "../src/redux/store";

export default class App extends Component {
    render() {
        return (
            <div data-testid="app">
                <Provider store={store}>
                    <Routes />
                </Provider>
            </div>
        );
    }
}