import React from "react";
import Home from "../screen/Home";
import Carousel from "../screen/Carousel";
import Signin from "../screen/Signin";
import Next from "../screen/Next";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

export default function Routes() {
  return (
    <div data-testid="routes">
      <Router>
        <Switch>
          <Route exact path="/">
            <Signin />
          </Route>
          <Route path="/home">
            <Home data-testid="Home"/>
          </Route>

          <Route path="/carousel">
            <Carousel />
          </Route>

          <Route path="/next">
            <Next />
          </Route>

          <Redirect to="/" />
        </Switch>
      </Router>
    </div>
  );
}
