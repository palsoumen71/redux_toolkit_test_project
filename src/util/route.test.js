import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Routes from "./route";

describe("Routes", () => {
  test("renders the Signin component on the default route", () => {
    render(
      <MemoryRouter initialEntries={["/"]}>
        <Routes />
      </MemoryRouter>
    );

    const signinComponent = screen.getByTestId("signin");
    expect(signinComponent).toBeInTheDocument();
  });


  test("renders the Home component on the /home route", () => {
    render(
      <MemoryRouter initialEntries={["/home"]}>
        <Routes />
      </MemoryRouter>
    );

    // const homeComponent = screen.getByTestId("home");
    // expect(homeComponent).toBeInTheDocument();
    expect(screen.queryByTestId('home')).not.toBeInTheDocument();
  });

  test("renders the Carousel component on the /carousel route", () => {
    render(
      <MemoryRouter initialEntries={["/carousel"]}>
        <Routes />
      </MemoryRouter>
    );

    // const carouselComponent = screen.getByTestId("carousel");
    // expect(carouselComponent).toBeInTheDocument();
    expect(screen.queryByTestId('carousel')).not.toBeInTheDocument();
  });

  test("renders the Next component on the /next route", () => {
    render(
      <MemoryRouter initialEntries={["/next"]}>
        <Routes />
      </MemoryRouter>
    );

    // const nextComponent = screen.getByTestId("next");
    // expect(nextComponent).toBeInTheDocument();
    expect(screen.queryByTestId('next')).not.toBeInTheDocument();
  });

  test("redirects to the Signin component for unknown routes", () => {
    render(
      <MemoryRouter initialEntries={["/unknown"]}>
        <Routes />
      </MemoryRouter>
    );

    const signinComponent = screen.getByTestId("signin");
    expect(signinComponent).toBeInTheDocument();
  });
});
