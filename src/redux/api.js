import { createAsyncThunk } from '@reduxjs/toolkit';

const API_BASE_URL = 'https://reqres.in/api';

export const fetchApiData = createAsyncThunk(
    'api/fetchApiData',
    async ({ method, endpoint, body }, thunkAPI) => {

        try {
            const options = {
                method,
                headers: { 'Content-Type': 'application/json' },
            };

            if (body) {
                options.body = JSON.stringify(body);
            }

            const response = await fetch(`${API_BASE_URL}/${endpoint}`, options);

            if (!response.ok) {
                const errorResponse = await response.json();
                return thunkAPI.rejectWithValue(errorResponse);
            }

            const data = await response.json();
            return data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error.response.data);
        }
    }
);
