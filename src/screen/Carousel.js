import React, { Component } from 'react';
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../screen/Carousel.css";




class Car extends Component {
  render() {
    return (

      <div>
        <Carousel className="fullscreen-carousel">
          <Carousel.Item>
            <img
              className="d-block w-100 h-100"
              src="https://source.unsplash.com/user/erondu/1600x900"
              alt="First slide"
            />
            <Carousel.Caption>
              <h3> </h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100 h-100"
              src="https://source.unsplash.com/user/erondu/1600x900"
              alt="Second slide"
            />

            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100 h-100"
              src="https://source.unsplash.com/user/erondu/1600x900"
              alt="Third slide"
            />

            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>



    );
  }

}

export default Car;