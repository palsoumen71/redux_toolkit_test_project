import React, { useState } from 'react'


export default function TextForm(props) {


    const [text, setText] = useState('');
    const [mode, setMode] = useState('light');

    const handleUpClick = () => {
        let newText = text.toUpperCase();
        setText(newText)
        alert("Converted to uppercase!", "success");
    }

    const handleLoClick = () => {
        let newText = text.toLowerCase();
        setText(newText)
        alert("Converted to lowercase!", "success");
    }

    const handleClearClick = () => {
        let newText = '';
        setText(newText);
        alert("Text Cleared!", "success");
    }

    const handleOnChange = (event) => {
        setText(event.target.value)
    }


    const handleCopy = () => {
        navigator.clipboard.writeText(text);
        alert("Copied to Clipboard!", "success");
    }


    const handleExtraSpaces = () => {
        let newText = text.split(/[ ]+/);
        setText(newText.join(" "));
        alert("Extra spaces removed!", "success");
    }

    const toggleMode = () => {
        if (mode === 'light') {
            setMode('dark');
            document.body.style.backgroundColor = '#042743';
            alert("Dark mode has been enabled", "success");
        }
        else {
            setMode('light');
            document.body.style.backgroundColor = 'white';
            alert("Light mode has been enabled", "success");
        }
    }


    return (
        <>
            <div className="container" style={{ color: props.mode === 'dark' ? 'white' : '#042743' }} data-testid="next">
                <div style={{ flexDirection: 'row', display: "flex", width: "100%" }}>
                    <div style={{ width: "60%", height: "30px" }}>
                        <h1 style={{ fontSize: '10px', textAlign: "center" }}>
                            Try Word_count - word counter, character counter, remove extra spaces
                        </h1>
                    </div>
                    <div style={{ width: "40%", height: "30px", alignItems: "center", justifyContent: "center" }} className={`form-check form-switch text-${props.mode === 'light' ? 'dark' : 'light'}`}>
                        <input className="form-check-input" onClick={() => toggleMode()} type="checkbox" id="flexSwitchCheckDefault" />
                        <label style={{fontSize:"10px"}} className="form-check-label" htmlFor="flexSwitchCheckDefault">Enable DarkMode</label>
                    </div>
                </div>
                <div className="mb-3">
                    <textarea className="form-control" value={text} onChange={handleOnChange} style={{ backgroundColor: props.mode === 'dark' ? '#13466e' : 'white', color: props.mode === 'dark' ? 'white' : '#042743' }} id="myBox" rows="8">

                    </textarea>
                </div>
                <button disabled={text.length === 0} className="btn btn-primary mx-1 my-1" onClick={handleUpClick}>
                    Convert to Uppercase
                </button>
                <button disabled={text.length === 0} className="btn btn-primary mx-1 my-1" onClick={handleLoClick}>
                    Convert to Lowercase
                </button>
                <button disabled={text.length === 0} className="btn btn-primary mx-1 my-1" onClick={handleClearClick}>
                    Clear Text
                </button>
                <button disabled={text.length === 0} className="btn btn-primary mx-1 my-1" onClick={handleCopy}>
                    Copy Text
                </button>
                <button disabled={text.length === 0} className="btn btn-primary mx-1 my-1" onClick={handleExtraSpaces}>
                    Remove Extra Spaces
                </button>
            </div>
            <div className="container my-3" style={{ color: props.mode === 'dark' ? 'white' : '#042743' }}>
                <h2>Your text summary</h2>
                <p>{text.split(/\s+/).filter((element) => { return element.length !== 0 }).length} words and {text.length} characters</p>
                <p>{0.008 * text.split(/\s+/).filter((element) => { return element.length !== 0 }).length} Minutes read</p>
                <h2>Preview</h2>
                <p>{text.length > 0 ? text : "Nothing to preview!"}</p>
            </div>
        </>
    )
}