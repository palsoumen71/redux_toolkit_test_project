import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Table, Button, Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, useHistory } from 'react-router-dom';
import '../App.css';
import { fetchApiData } from '../redux/api';

export default function Home() {

  const { users, isLoading, error } = useSelector((state) => state.user);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const checkLogin = () => {
      const isLoggedIn = localStorage.getItem('isLogged');

      if (isLoggedIn === 'false') {
        history.push('/');
      }
    };

    dispatch(fetchApiData({ method: 'GET', endpoint: 'users' }));
    checkLogin();
  }, [dispatch, history]);



  const logout = () => {
    localStorage.setItem('isLogged', 'false');
    history.push('/');
  };

  if (isLoading) {
    return <div data-testid="home">Loading...</div>;
  } else if (error) {
    return <div data-testid="home">Error: {error}</div>;
  } else {
    return (
      <Container fluid data-testid="home">
        <Row>
          <Col>
            <div className="Login">
              <Table responsive>
                <thead>
                  <tr>
                    <th>id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>avatar</th>
                  </tr>
                </thead>
                <tbody>
                  {users.data && users.data.map((user) => (
                    <tr key={user.id}>
                      <td>{user.id}</td>
                      <td>{user.first_name}</td>
                      <td>{user.last_name}</td>
                      <td>{user.email}</td>
                      <td>{user.avatar}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
              <Button className="b1" variant="link">
                <Link to="/Carousel">Carousel</Link>
              </Button>
              <Button className="b1" variant="link">
                <Link to="/next">Next</Link>
              </Button>
              <Button onClick={logout} className="b1" variant="link">
                Log out
              </Button>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}
