import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "../App.css";
import { useHistory } from "react-router-dom";

export default function Signin() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const history = useHistory()

    const onEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const onPasswordChange = (e) => {
        setPassword(e.target.value);
    };

    useEffect(() => {
        const checkLogin = () => {
            const isLoggedIn = localStorage.getItem("isLogged");

            if (isLoggedIn === "true") {
                history.push("/home");
            }
        };

        checkLogin();
    }, [history]);

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        if (email === "admin@mail.com" && password === "admin") {
            localStorage.setItem("isLogged", 'true')
            history.push("/home");
        } else {
            alert("please input correct credentials!");
        }
    };


    return (
        <div className="Login" data-testid="signin">
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="img" className="d-flex justify-content-center align-items-center">
                    <img src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" width="52" height="52" margin="50" max-width='320' alt="Bootstrap logo" />
                </Form.Group>
                <Form.Group className='pl-3 pr-2'><h1 className="h3 mb-3 font-weight-normal">Please sign in</h1></Form.Group>

                <Form.Group size="lg" controlId="email" className='pl-3 pr-2'>
                    <Form.Control
                        placeholder="Email address"
                        autoFocus
                        type="email"
                        value={email}
                        onChange={(e) => onEmailChange(e)}
                        required
                    />
                </Form.Group>
                <Form.Group size="lg" controlId="password" className='pl-3 pr-2'>
                    <Form.Control
                        placeholder="Password"
                        type="password"
                        value={password}
                        onChange={(e) => onPasswordChange(e)}
                        required
                    />
                </Form.Group>

                <Form.Group type="checkbox" value="remember-me" className='pl-3 pr-2'>
                    <Form.Label>
                        <input type="checkbox" value="remember-me"></input>Remember Me
                    </Form.Label>
                </Form.Group>
                <Form.Group className='pl-3 pr-2'>
                    <Button block size="lg" type="submit" disabled={!validateForm()} variant="primary">
                        Sign in
                    </Button>
                </Form.Group>
                <Form.Group className='pl-3 pr-2'>
                    <p className="text-muted">© 2023-2024</p>
                </Form.Group>
            </Form>
        </div>
    );
}