import React from "react";
import { render } from "@testing-library/react";
import App from "./App";
import { Provider } from "react-redux";
import store from "../src/redux/store";
import Routes from "./util/route";
import configureStore from "redux-mock-store";

test("renders the App component", () => {
  render(<App />);
});


// test("renders the App component with the correct store prop", () => {
//   // Create a mock store with an initial state
//   const initialState = {
//     users: [],
//     isLoading: true,
//     error: null,
//   }; // You can add any initial state here if needed

//   const mockStore = configureStore([])(initialState);

//   // Render the App component with the mock store wrapped in the Provider
//   const { getByTestId } = render(
//     <Provider store={mockStore}>
//       <App />
//     </Provider>
//   );

//   // Query the root element of the App component using data-testid
//   const appRootElement = getByTestId("app");

//   // Access the store prop from the Provider
//   const providerStore = appRootElement.props.store;

//   // Compare the providerStore with the mockStore
//   expect(providerStore).toBe(mockStore);
// });




test("renders the Routes component inside the Provider", () => {
  const { getByTestId } = render(
    <Provider store={store}>
      <Routes />
    </Provider>
  );

  expect(getByTestId("routes")).toBeInTheDocument();
});

